public class Main {


    public static int partition(int[] arr, int low, int high, int x) {
        int pivot = arr[high];

        int i = low - 1;
        for (int index = low; index < high; index++) {
            // Custom codition compare
            if (Math.abs(x - arr[index]) <= (Math.abs(x - pivot))) {
                i++;
                int temp = arr[i];
                arr[i] = arr[index];
                arr[index] = temp;
            }
        }

        int temp1 = arr[i + 1];
        arr[i + 1] = arr[high];
        arr[high] = temp1;
        return i + 1;
    }

    public static void sort(int[] arr, int low, int high, int x) {
        if (low < high) {
            int pi = partition(arr, low, high, x);
            sort(arr, low, pi - 1, x);
            sort(arr, pi + 1, high, x);
        }
    }


    public static void main(String[] args) {
        int[] arr = {10, 5, 3, 9, 2};
        int x = 7;
        sort(arr, 0, arr.length -1, x);

        for (int value: arr) {
            System.out.println(value);
        }
    }
}
